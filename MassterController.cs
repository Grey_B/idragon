﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MassterController : MonoBehaviour {

    public Text helloText; // text where i place game state and some info
    public byte enemyCout; // Count of enemy in match

    private byte gameMode; // global game state 1 - geting cards 2 - ASM turn 3 - D turn 4 - R turn 5 - regenerations cards 6 - end of the game
    private float currentTime;
    private GameObject[] pilots; // array of pilots

    // Use this for initialization
    void Start () {
        gameMode = 0;
        helloText.text = "Welcome";
        currentTime = Time.time;
        pilots = GameObject.FindGameObjectsWithTag("Pilot");
    }
	
	// Update is called once per frame
	void Update () {
        // for now thanging state at timer
        // ToDo: rewrite on events
        if (Time.time - currentTime >= 5 && gameMode <= 6)
        {
            gameMode++;
            helloText.text = "Current mode is " + gameMode.ToString();
            foreach (GameObject pilot in pilots)
            {
                PilotController pilotController;
                pilotController = pilot.GetComponent<PilotController>();
                pilotController.Action(gameMode);
            }
            currentTime = Time.time;
        }
    }
}
