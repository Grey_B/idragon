﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Pilot class template
public class PilotController : MonoBehaviour {

    public Text output; // for test

    public List<GameObject> altitudeDeck = new List<GameObject>(); // array of cards
    public List<GameObject> speedDeck = new List<GameObject>(); // array of cards
    public List<GameObject> maneurDeck = new List<GameObject>(); // array of cards
    public List<GameObject> fireDeck = new List<GameObject>(); // array of cards

    public int deckSize;

    List<GameObject> altitudeHand = new List<GameObject>(); // array of cards
    List<GameObject> speedHand = new List<GameObject>(); // array of cards
    List<GameObject> maneurHand = new List<GameObject>(); // array of cards
    List<GameObject> fireHand = new List<GameObject>(); // array of cards
    List<GameObject> randomHand = new List<GameObject>(); // array of cards

    void Shuffle(List<GameObject> cards)
    {
        for (int i = cards.Count - 1; i > 0; i--)
        {
            int j = Random.Range(0, i);
            if (i != j)
            {
                GameObject tmp = cards[i];
                cards[i] = cards[j];
                cards[j] = tmp;
            }
        }
    }

    private void Start()
    {
        // For testing. Point that Pilot class do something
        output = GameObject.Find("helloText").GetComponent<Text>();
        Shuffle(altitudeDeck);
        Shuffle(speedDeck);
        Shuffle(maneurDeck);
        Shuffle(fireDeck);
    }

    /// <summary>
    /// Main procedure
    /// </summary>
    /// <param name="gameMode">Current game node</param>
    public void Action(byte gameMode)
    {
        switch (gameMode)
        {
            case 1:
                GetCards();
                break;
            case 2:
                MakeTurn();
                break;
            case 3:
                MakeDamageTurn();
                break;
            case 4:
                MakeRandomTurn();
                break;
        }
    }

    /// <summary>
    /// Method that realise getting cards from deck
    /// </summary>
    void GetCards()
    {
        // here be dragons
        output.text = "AI geting cards" + altitudeDeck.Count.ToString();
    }

    /// <summary>
    /// Method that making game turn
    /// </summary>
    void MakeTurn()
    {
        // here be dragons
        output.text = "AI playing card";
    }

    /// <summary>
    /// Method that playing F-cards
    /// </summary>
    void MakeDamageTurn()
    {
        // here be dragons
        output.text = "AI playing FIRE card";
    }

    /// <summary>
    /// Method that playing R-cards
    /// </summary>
    void MakeRandomTurn()
    {
        // here be dragons
        output.text = "AI playing RANDOM card";
    }
}
