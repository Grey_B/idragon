﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeckController : MonoBehaviour {

    // Make this private in future
    public List<GameObject> cards = new List<GameObject>(); // array of cards
    public GameObject emptyCard; // Default card to return when there are no cards in the deck

    /// <summary>
    /// Shuffle this Deck
    /// </summary>
    public void Shuffle()
    {
        for (int i = cards.Count - 1; i > 0; i--)
        {
            int j = Random.Range(0, i);
            if (i != j)
            {
                GameObject tmp = cards[i];
                cards[i] = cards[j];
                cards[j] = tmp;
            }
        }
    }

    /// <summary>
    /// Geting card from top of the deck
    /// </summary>
    /// <returns>card from top or default emptyCard</returns>
    public GameObject GetCard()
    {
        GameObject card;
        if (cards.Count > 0)
        {
            card = cards[0];
            cards.RemoveAt(0);
            return card;
        }
        return emptyCard;
    }

    // Use this for initialization
    void Start ()
    {
        Shuffle();
    }
}
